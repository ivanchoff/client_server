// broker para la aplicacion de musica
// choff@riseup.net
// primero se inicia el broker, luego los workers

#include<bits/stdc++.h>
#include<czmq.h>

using namespace std;

unordered_map<string,int> workload;
vector<char *> workers;
string promos="";
string catalogo="";
int statusControl;

int maxServed=1000000; 
char* freeWorker;     //id of freeworker for replicate

char* workerSource;   //worker that start replications
char* songReplicate;  //name song to replicate
zframe_t* file;       //file to replicate

void getWorkerStatus(void * toworker){
  for(int i=0; i< workers.size(); i++){
    zmsg_t* msgstatus = zmsg_new();
    zmsg_addstr(msgstatus,workers[i]);
    zmsg_addstr(msgstatus,"status");
    zmsg_send(&msgstatus,toworker);
  }
  statusControl=workers.size();
}

void replicate(void* toworker){
  cout<<"replicar ....................."<<endl;
  cout<<"freeWorker:    "<<freeWorker<<endl;
  cout<<"workerSource:  "<<workerSource<<endl;
  cout<<"songreplicate: "<<songReplicate<<endl;
  zmsg_t* rep = zmsg_new();
  zmsg_addstr(rep,freeWorker);
  zmsg_addstr(rep,"copySong");
  zmsg_addstr(rep,songReplicate);
  zmsg_append(rep,&file);
  zmsg_send(&rep,toworker);
  
  zmsg_t* rep1 = zmsg_new();
  zmsg_addstr(rep1,workerSource);
  zmsg_addstr(rep1,"reset");
  zmsg_addstr(rep1,songReplicate);
  zmsg_send(&rep1,toworker);
  
  maxServed=1000000;
  free(freeWorker);
  free(workerSource);
  free(songReplicate);
}
// msg from worker
// | id | op | file |
void handleWorkerMsg(zmsg_t* msg, void* worker){
  cout<<"Handling this msg from worker"<<endl;
  zmsg_print(msg);
  char* id = zframe_strdup(zmsg_pop(msg));

  char *op = zmsg_popstr(msg);
  if(strcmp(op,"register")==0){      //worker registration
    workers.push_back(id);
    workload[id]=0;
    zmsg_t * message = zmsg_new();
    zmsg_pushstr(message,id);
    zmsg_addstr(message,"list");
    cout<<"sending to worker this:"<<endl;
    zmsg_print(message);
    zmsg_send(&message,worker);
  }
  if(strcmp(op,"list")==0){         //recieving list
    string list = zmsg_popstr(msg);
    string promo = zmsg_popstr(msg);
    list+="\n";
    promo+="\n";
    catalogo += list;
    promos += promo; 
    cout<<"promo:"<<endl<<promo<<endl;
    FILE* file = fopen("./data/Catalogo","a+");
    FILE* file2 = fopen("./data/publicity","a+");
    fprintf(file,list.c_str());
    fprintf(file2,promo.c_str());
    fclose(file);
    fclose(file2);
  }
  if(strcmp(op,"reply")==0){
    songReplicate = zframe_strdup(zmsg_pop(msg));
    file = zframe_dup(zmsg_pop(msg));
    workerSource = id;
    getWorkerStatus(worker);

  }
  if(strcmp(op,"status")==0){
    int tmp = atoi(zmsg_popstr(msg));
    workload[id] = tmp;
    if(tmp < maxServed){
      maxServed=tmp;
      freeWorker = id;
    }
    statusControl--;
    cout<<"statusControl: "<<statusControl<<endl;
    if(statusControl==0){
      replicate(worker);
    }
  }
  zmsg_destroy(&msg);
}
void handleClientMsg(zmsg_t* msg, void* client){
  cout<<"Handling this form Client..."<<endl;
  zmsg_print(msg);
  zframe_t *id = zmsg_pop(msg);
  char *op = zmsg_popstr(msg);

  if(strcmp(op,"list")==0){
    zmsg_prepend(msg,&id);
    zmsg_addstr(msg,"list");
    zmsg_addstr(msg, catalogo.c_str());
    zmsg_addstr(msg, promos.c_str());
    cout<<"promos"<<promos<<endl;
    zmsg_send(&msg,client);
  }
}
//exec: ./broker id
int main(int argc, char** argv){
  cout<<"Broker agent starting..."<<endl;
  zctx_t* context = zctx_new();
  void* worker = zsocket_new(context,ZMQ_ROUTER);
  void* client = zsocket_new(context,ZMQ_ROUTER);
  
  if(argc > 1){
    char *id=argv[1];
    zsocket_set_identity(worker,id);
  }
  
  zsocket_bind(worker,"tcp://*:5556"); 
  zsocket_bind(client,"tcp://*:5555");
  cout<<"binding to tcp://*:[5556-5555] with id:"<<argv[1]<<endl;

  zmq_pollitem_t items[]={{worker,0,ZMQ_POLLIN,0},
                          {client,0,ZMQ_POLLIN,0}};

  //create file catalog container
  FILE * file = fopen("./data/Catalogo","w");
  fclose(file);

  while(true){
    zmq_poll(items,2,10*ZMQ_POLL_MSEC);
    if(items[0].revents & ZMQ_POLLIN){//msg from worker
      zmsg_t* msg = zmsg_recv(worker);
      handleWorkerMsg(msg,worker);
    }
    if(items[1].revents & ZMQ_POLLIN){//msg from client
      zmsg_t* msg = zmsg_recv(client);
      handleClientMsg(msg,client);
    }  
  }  
  zctx_destroy(&context);
  return 0;
}

