//  worker forf FreeSound
//  choff@riseup.net

#include<bits/stdc++.h>
#include<czmq.h>

using namespace std;

unordered_map<string,int> carga;
//char* fileSongs;
string pathSongs = "./data/music/";
string mySongs;  //primer dato ip luego las canciones.
string myPromos;
string identity;
double served=0;
double limit=0.2;  //limite para replicar cancion
int limit_inf_served=4;   

//script para listar canciones primer cadena la ip
void directoryList(){
  string cmd="bash scriptLS.sh "+identity;
  system(cmd.c_str());
}
void printContainer(){
  for(auto i: carga){
    cout<<i.first<<" -> "<<i.second<<endl;
  }
}
//reset statistics
void resetContainer(){
  for(auto i: carga){
    i.second=0;
  }
}
//load songs struct
void loadContainer(){
  stringstream ss(mySongs);
  string ip,tmp;
  int i=0;
  ss>>ip;
  while(ss>>tmp) carga[tmp]=0;
}
//manejador de carga, decide cuado replicar cancion
void loadHandler(char *song,void* broker){
  double x;
  x = carga[song] / served;
  if(x>limit && served > limit_inf_served){
    cout<<"comienzo a replicar"<<endl;
    zmsg_t* msg =zmsg_new();
    zmsg_addstr(msg,"reply");
    zmsg_addstr(msg,song);
    zfile_t* file = zfile_new("./data/music/",song);
    cout<<"File is readable? "<< zfile_digest(file) << endl;
    zfile_close(file);
    string path = pathSongs+song;
    zchunk_t *chunk = zchunk_slurp(path.c_str(),0);
    if(!chunk) {
      cout << "Cannot read file!" << endl;
      return;
    }
    cout << "Chunk size: " << zchunk_size(chunk) << endl;
    
    zframe_t *frame=zframe_new(zchunk_data(chunk), zchunk_size(chunk));
    zmsg_append(msg,&frame);
    zmsg_print(msg);
    zmsg_send(&msg, broker);
  }
}
void handleClientMsg(zmsg_t* msg, void* toclient, void* broker) {
  cout<<"handling this from client..."<<endl;
  zmsg_print(msg);
  zframe_t* id = zframe_dup(zmsg_pop(msg));
  char * op = zmsg_popstr(msg);
  cout<<"op: "<<op<<endl;
  if(strcmp(op,"ping")==0){
    zmsg_prepend(msg,&id);
    zmsg_addstr(msg,"pong");
    zmsg_send(&msg,toclient);
  }
  if(strcmp(op,"song")==0){
    char* song = zmsg_popstr(msg);
    cout<<"song name ./data/music/"<<song<<endl;
    zfile_t* file = zfile_new("./data/music/",song);
    cout<<"File is readable? "<< zfile_digest(file) << endl;
    zfile_close(file);
    string path = pathSongs;
    path+=song;
    cout<<"path:"<<path<<endl;
    zchunk_t *chunk = zchunk_slurp(path.c_str(),0);
    if(!chunk) {
      cout << "Cannot read file!" << endl;
      return;
    }
    cout << "Chunk size: " << zchunk_size(chunk) << endl;
    
    zframe_t *frame=zframe_new(zchunk_data(chunk), zchunk_size(chunk));
    zmsg_t* res = zmsg_new();
    zmsg_prepend(res,&id);
    zmsg_addstr(res,"song");
    zmsg_addstr(res,song);
    zmsg_append(res,&frame);
    cout<<"sendind this to client..."<<endl;
    zmsg_print(msg);
    zmsg_send(&res, toclient);
    
    served++;
    carga[song]++;
    
    loadHandler(song,broker);

  }
}

void handleServerMsg(zmsg_t* msg, void* server) {
  cout<<"handling this from server..."<<endl;
  zmsg_print(msg);
  
  char* op = zmsg_popstr(msg);
  if(strcmp(op,"list")==0){
    zmsg_t* message = zmsg_new();
    zmsg_addstr(message,"list");
    string fsongs="./data/songs"+identity;
    string fpromo="./data/publicity"+identity;
    ifstream file(fsongs.c_str()); 
    ifstream filePromo(fpromo.c_str());
    string tmp;
    getline(file,tmp);
    mySongs+=tmp;
    zmsg_addstr(message,mySongs.c_str());
    getline(filePromo,tmp);
    myPromos+=tmp;
    zmsg_addstr(message,myPromos.c_str());
    cout<<"sending this to broker"<<endl;
    zmsg_print(message);
    zmsg_send(&message,server);
    loadContainer();
    printContainer();
   }
  if(strcmp(op,"status")==0){
    zmsg_t* res = zmsg_new();
    zmsg_addstr(res,"status");
    zmsg_addstr(res,to_string(served).c_str());
    zmsg_send(&res,server);
  }
  if(strcmp(op,"copySong")==0){
    char * song = zmsg_popstr(msg);
    zframe_t *file = zmsg_pop(msg);

    zfile_t* download = zfile_new("./data/music/",song);
    zfile_output(download);
    zchunk_t* chunk = zchunk_new(zframe_data(file),zframe_size(file));
    zfile_write(download,chunk,0);
    zfile_close(download);
    served=0;
    resetContainer();
  }
  if(strcmp(op,"reset")==0){
    string s = zmsg_popstr(msg); 
    served-=carga[s];
    carga[s]=0;
    printContainer();
    //served=0;
    //resetContainer();
  }
  zmsg_destroy(&msg);
}

// exec: ./worker id ipbroker ownip
int main(int argc, char** argv) {
  cout << "Worker agent starting..." << endl;
  zctx_t* context = zctx_new();
  void* toserver = zsocket_new(context, ZMQ_DEALER);  //to broker
  void* toclient = zsocket_new(context, ZMQ_ROUTER);  //to client
  string ip;
  string ipbroker;
	if (argc > 1) {
		identity = argv[1];
    //fileSongs = argv[2];
    ipbroker = argv[2];
    ip = argv[3];
    mySongs+=ip;
    mySongs+=" ";
    myPromos+=mySongs;
    zsocket_set_identity(toserver,identity.c_str());
    zsocket_set_identity(toclient,identity.c_str());
  }

  directoryList();

  cout<<ipbroker<<endl;
  //int s = zsocket_connect(toserver,"tcp://localhost:5556");
  int s = zsocket_connect(toserver,ipbroker.c_str());
  cout << "connecting to server: " << (s == 0 ? "OK" : "ERROR") << endl;
  
  int c = zsocket_bind(toclient,"tcp://*:5557");
  //cout << "c: "<<c<<"srt:"<<socketClient<<endl;
  cout << "listening clients at: "<<c<< endl;

  //Register the worker with the server
  zmsg_t* regmsg = zmsg_new();
  zmsg_addstr(regmsg, "register");
  cout<<"Sending this to broker..."<<endl;
  zmsg_print(regmsg);
  zmsg_send(&regmsg, toserver);
  
  zmq_pollitem_t items[] = {{toserver, 0, ZMQ_POLLIN, 0},
                            {toclient, 0, ZMQ_POLLIN, 0}};
     
  while (true) {
    zmq_poll(items, 2, 10 * ZMQ_POLL_MSEC);
    if (items[0].revents & ZMQ_POLLIN) {
      zmsg_t* msg = zmsg_recv(toserver);
      handleServerMsg(msg, toserver);
    }
    if (items[1].revents & ZMQ_POLLIN) {
      zmsg_t* msg = zmsg_recv(toclient);
      handleClientMsg(msg, toclient, toserver); 
    }
  } 
  zctx_destroy(&context);
  return 0;
}
