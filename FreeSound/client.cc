//  client forpp  FreeSound
//  choff@riseup.net

#include<bits/stdc++.h>
#include<czmq.h>
#include <thread>
#include <chrono>
#include <ao/ao.h>
#include <mpg123.h>

using namespace std;
unordered_map<string,vector<string> > promos;
unordered_map<string,vector<string> > catalog; 
unordered_map<int,string> options;  //search result
unordered_map<int,string> optionsPromos;
void* toworker;
void* tobroker;
char* identity;
string ip;
list<string> songlist;              //list of songs
int playlst=1; // 0:stop, 1=play;
int flag=0;    // control for receivin songs 
int PlayedSongs=0;                 
int timesForPromos=3;
int quit=0;
#define BITS 8


class SimplePlayer {
private:
  int driver;
  mpg123_handle *mh;
  ao_device* dev;
  //unsigned char *buffer;
  size_t buffer_size;
  bool stop_;
  
public:
  SimplePlayer(void) {
    ao_initialize();
    driver = ao_default_driver_id(); 
    mpg123_init(); 
    int err;
    mh = mpg123_new(NULL, &err);
    buffer_size = mpg123_outblock(mh);
    stop_ = false;
  }
  
  ~SimplePlayer(void) {
    //free(buffer);
    if (dev)
      ao_close(dev);
    mpg123_close(mh);
    mpg123_delete(mh);
    mpg123_exit();
    ao_shutdown();
  }
  
 static void actualPlay(mpg123_handle *mh, ao_device* dev, size_t size, const bool& stop) {
    /* decode and play */
    size_t done;
    unsigned char *buffer = (unsigned char*) malloc(size * sizeof(unsigned char));
    while (mpg123_read(mh, buffer, size, &done) == MPG123_OK && !stop) {
      char * b = (char*)buffer;
      ao_play(dev, b, done);
    }
    free(buffer);
    ao_close(dev);
    mpg123_close(mh);
  }

  //std::thread play(const char* fname) {
  void play(const char* fname) {
    /* open the file and get the decoding format */
    mpg123_open(mh, fname);
    int channels, encoding;
    long rate;
    mpg123_getformat(mh, &rate, &channels, &encoding);
    
    /* set the output format and open the output device */
    ao_sample_format format;
    format.bits = mpg123_encsize(encoding) * BITS;
    format.rate = rate;
    format.channels = channels;
    format.byte_format = AO_FMT_NATIVE;
    format.matrix = 0;
    dev = ao_open_live(driver, &format, NULL);
    
    //return std::thread(actualPlay, mh,dev,buffer_size,stop_);
    actualPlay(mh,dev,buffer_size,stop_);
  }
  
  void stop(void) {
    stop_ = true;
  }
  
 
};
SimplePlayer player;

void printSongList(){
  cout<<"********************************"<<endl;
  cout<<"***** Canciones en lista: ******"<<endl;
  for(auto it=songlist.begin();  it!=songlist.end(); ++it){
    cout<<"----> "<<*it<<endl;
  }
  cout<<endl;
}
void printall(){
  cout<<"Promos:"<<endl;
  for(auto it = promos.begin(); it != promos.end(); ++it){
    cout<< it->first<< ": ";
    for(int i=0; i<it->second.size(); i++){
      cout<<it->second[i]<<", ";
    }
    cout<<endl;
  }
 
  cout<<"catalog: "<<endl;
  for(auto it = catalog.begin(); it != catalog.end(); ++it){
    cout<< it->first<< ": ";
    for(int i=0; i<it->second.size(); i++){
      cout<<it->second[i]<<", ";
    }
    cout<<endl;
  }
  
}

//parser for songs, promos
void parser(string data, string container){
  stringstream ss;
  string line,tmp;
  ss<< data;
  while(getline(ss,line)){
    istringstream iss(line);
    string dir;
    iss>>dir;     //ip
    while(iss>>tmp){
      if(container == "promo"){
        promos[tmp].push_back(dir);
      }
      if(container == "catalog"){
        catalog[tmp].push_back(dir);
      }
    }
  }
  if(container == "promo"){
    int i=0;
    for(auto it: promos){
      optionsPromos[i++]=it.first;
    }
  }
}
void handleWorkerMsg(zmsg_t* msg, void* worker) {
  //cout<<"handling this from worker..."<<endl;
  //zmsg_print(msg);
  char* op = zmsg_popstr(msg);
  if(strcmp(op,"song")==0){
    char * song = zmsg_popstr(msg);
    zframe_t *file = zmsg_pop(msg);
    string old="./data/music/received/";
    zfile_t* download = zfile_new("/tmp/",song);
    zfile_output(download);
    zchunk_t* chunk = zchunk_new(zframe_data(file),zframe_size(file));
    //cout << "Chunk size: " << zchunk_size(chunk) << endl;
    //cout<<"File is readable? "<< zfile_digest(file) << endl;
    zfile_write(download,chunk,0);
    zfile_close(download);
    flag=1;
    //cout<<"flag en 1?: "<<flag<<endl;
  }
  zmsg_destroy(&msg);
  
}

void handleBrokerMsg(zmsg_t* msg, void* server) {
  //cout<<"handling this from broker..."<<endl;
  //zmsg_print(msg);
  char* op = zmsg_popstr(msg);
  if(strcmp(op,"list")==0){
    string catalogo = zmsg_popstr(msg);
    string promo = zmsg_popstr(msg);
    //cout<<"catalogo:"<<endl<<catalogo<<endl;
    //parseCatalog(catalogo);
    parser(catalogo,"catalog");
    parser(promo,"promo");
    //printall();
  }
  zmsg_destroy(&msg);
}

string search(){
  int k=0,op=0;
  string cad;
  cout<<"Ingrese nombre de la cancion: "<<endl;
  cin>>cad;
  string res="";
  options.clear();
  for(auto it = catalog.begin(); it != catalog.end(); ++it){
    string key = it->first;
    //cout<<it->first<<endl;
    if(key.find(cad) != string::npos){
      options[k++]=key;
      res+=to_string(k-1);
      res+=" - ";
      res+= key;
      res+="\n";
    }
  }
  return res;
}


string getWorker(string key, string container){
  string result;
  if(container=="catalog"){
    int pos = rand() % catalog[key].size();
    result = catalog[key][pos];
  }
  if(container=="promos"){
    int pos = rand() % promos[key].size();
    result = promos[key][pos];
  }
  return result;
}

// params : songname, ip
void getSong(string cad, string newip){
  //cout<<"get song: "<<cad<<" "<<newip<<endl;
  int d = zsocket_disconnect (toworker, ip.c_str());
  cout<<"disconnect "<<((d==0)? "OK" : "ERROR" )<<endl;
  int c = zsocket_connect(toworker, newip.c_str());
  cout<<"connecting to worker: "<<(c == 0 ? "OK" : "ERROR")<<endl;
  ip = newip;

  zmsg_t* msg = zmsg_new();
  zmsg_addstr(msg,"song");
  zmsg_addstr(msg,cad.c_str());
  //cout<<"sending this to  worker"<<ip<<endl;
  //zmsg_print(msg);
  zmsg_send(&msg,toworker);
  flag=0;
}


void playList(){
  list<string>::iterator it = songlist.begin();
  int itpromos=0;
  while(playlst!=0 && songlist.size()>0){
    string menu="1 - siguiente\n2 - anterior\n0 - salir\n ";
    printSongList();
    if(timesForPromos==PlayedSongs){
      if(itpromos>=optionsPromos.size()){
        itpromos=0;
      }
      string promo_name = optionsPromos[itpromos++];
      string ip_promo = getWorker(promo_name,"promos");
      //cout<<"promo a descargar: "<<promo_name<<" "<<ip_promo<<endl;
      getSong(promo_name,ip_promo);
      while(!flag){}
      string cmd = "/tmp/"+promo_name;
      player.play(cmd.c_str());
      PlayedSongs=0;
    }else{
      //cout<<"cancion a descargar: "<<*it;
      string newip = getWorker(*it,"catalog");
      string cmd = "/tmp/"+*it;
      getSong(*it,newip);
        
      while(!flag){}
      player.play(cmd.c_str());
      PlayedSongs++;
      cout<<menu<<endl;
      playlst=0;
      cin>>playlst;
      switch(playlst){
      case 1:
        if(it==songlist.end()){
          cout<<"no hay mas elementos en la lista"<<endl;
          //cout<<"size: "<<songlist.size()<<"it:"<<*it<<endl;
          songlist.clear();
          playlst=0; 
        }else{
          it++;
        }
        break;
      case 2:
        if(it!=songlist.begin()){ 
          *it--;
        }else{
          cout<<"no hay elemento anterior";
          if(it!=songlist.end()) *it++;
        }
        break;
      case 0:
        //songlist.clear();
        break;
      }
    }
  }
}


void update(void* socket){
  zmsg_t* regmsg = zmsg_new();
  zmsg_addstr(regmsg, "list");
  cout<<"Request list  to broker..."<<endl;
  //zmsg_print(regmsg);
  zmsg_send(&regmsg, socket);
  //printall();
}
void menu(void* broker){
  int op=1;
  string cad;
  //thread t2(playList,0);
  while(op!=0){
    cout<<endl<<"************************************"<<endl;
    cout<<"******** FreeSound Options:  *******"<<endl;
    cout<<"1 - Search"<<endl;
    cout<<"2 - View List"<<endl;
    cout<<"3 - play list"<<endl;
    cout<<"4 - actualizar catalogo"<<endl;
    cout<<"0 - Quit"<<endl;
    cin>>op;
    if(op==1){ // search
      string r= search();
      if(r.size()>0){
        int op2;
        cout<<"------------------------------------"<<endl;
        cout<<"----------   RESULTADO: ------------"<<endl<<r<<endl;
        cout<<"agregar cancion (numero): "<<endl;
        cin>>op2;
        songlist.push_back(options[op2]); //add song to list    
        //getSong(options[op2]);
      }else{
        cout<<"------------------------------------"<<endl;
        cout<<"no se encontro nada."<<endl;
      }
    }
    if(op==2){ //view list
      printSongList();
    }
    if(op==3){ //play list
      playlst=1;
      playList();
    }
    if(op==0)quit=1;
    if(op==4)update(broker);
  }
}


// exec: ./client id ipbroker
int main(int argc, char** argv) {
  system("cat banner");
  cout << "client agent starting..." << endl;
  zctx_t* context = zctx_new();
  tobroker = zsocket_new(context, ZMQ_DEALER);   //to broker
  toworker = zsocket_new(context, ZMQ_DEALER);   //To worker
	if (argc > 1) {
		identity = argv[1];
    ip = argv[2];
    zsocket_set_identity(tobroker,identity);
    zsocket_set_identity(toworker,identity);
  }
  
  int s = zsocket_connect(tobroker,ip.c_str());
  //cout << "connecting to server: " << (s == 0 ? "OK" : "ERROR") << endl;
  int c = zsocket_connect(toworker,ip.c_str());
  //cout<<"connecting to worker at: "<<(c == 0 ? "OK" : "ERROR")<<endl;
  
  update(tobroker);

  thread t1(menu,tobroker);

  while (!quit) {
    zmq_pollitem_t items[] = {{tobroker, 0, ZMQ_POLLIN, 0},
                            {toworker, 0, ZMQ_POLLIN, 0}};
  
    zmq_poll(items, 2, 10 * ZMQ_POLL_MSEC);
    if (items[0].revents & ZMQ_POLLIN) {
      zmsg_t* msg = zmsg_recv(tobroker);
      handleBrokerMsg(msg, tobroker);
    }
    if (items[1].revents & ZMQ_POLLIN) {
      zmsg_t* msg = zmsg_recv(toworker);
      handleWorkerMsg(msg, toworker);
    }
    //menu();
  }
  t1.join();
  zctx_destroy(&context);
  return 0;
}
