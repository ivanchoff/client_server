#include<bits/stdc++.h>
#include <queue>
#include <iostream>
#include <set>
#include <string>
#include <vector>
#include <unordered_map>
#include <czmq.h>
#include <string.h>

using namespace std;

typedef unordered_map<string, pair<int,vector<pair<zframe_t*,int>> >> WorkerReferences;
WorkerReferences wr;

queue<zmsg_t *> myqueue;

void registerWorker(zframe_t* id, string operation) {
  zframe_print(id, "Id to add");
  zframe_t* dup = zframe_dup(id);
  zframe_print(dup, "Id copy add");
	if(wr.count(operation)>0){
		wr[operation].second.push_back(make_pair(dup,0));
	}else{
		vector<pair<zframe_t*,int>> id;
		id.push_back(make_pair(dup,0));
		wr[operation]=make_pair(0,id);
	}
  cout << "Worker summary" << endl;
  for (const auto& e : wr) {
    cout << e.first << endl;
    for (auto& id : e.second.second) {
      char* reprId = zframe_strhex(id.first);
      cout << reprId << " ";
      free(reprId);
    }
    cout << endl;
  }
}

// method: 
// 1 - random selection
// 2 - selection almost like round robin.
// 3 - selection round robin and checkin state of worker
// 4 - select the next free worker.  
zframe_t* getWorkerFor(string operation, int method) {
	zframe_t* wid; int x,flag=0, size=0, pos, min=2<<30;
	switch(method){
	case 1:
		x= rand()% wr[operation].second.size();
		wid = wr[operation].second[x].first;
		flag=1;
		break;
		
	case 2:
		x = wr[operation].first;
		wid = wr[operation].second[x].first;
		flag=1;
		wr[operation].first++;
		if(wr[operation].first % wr[operation].second.size() == 0)
			wr[operation].first=0;
		break;
		
	case 3:
		size=wr[operation].second.size();
		x = wr[operation].first;
		for(int i=0; i<size;i++){
			pos = (x+i) % size;
			//cout<<wr[operation].second[pos].first<<" pos:"<<pos<<"second"<<wr[operation].second[pos].second<<endl;
			if(wr[operation].second[pos].second==0 ){
				wid = wr[operation].second[pos].first;
				break;
			}else{
				if(wr[operation].second[pos].second < min){
					wid = wr[operation].second[pos].first;
					min = wr[operation].second[pos].second;
				}
			}
		}
		flag=1;
		wr[operation].first++;
		break;
		
	case 4:
		size = wr[operation].second.size();
		x = wr[operation].first;
		for(int i=0; i<size;i++){
			pos = (x+i) % size;
			if(wr[operation].second[pos].second==0 ){
				wid = wr[operation].second[pos].first;
				flag=1;
				break;
			}
		}
		if(flag)wr[operation].first++;
		break;
	}

	if(flag)
		return zframe_dup(wid);
	return NULL;
}
// revisa la pila si hay mensajes además revisa si hay workers
// disponibles para atender el mensaje.
void queueManage(void* workers){	
	if(!myqueue.empty()){
		zmsg_t* msg =  zmsg_dup(myqueue.front());
		zframe_t* idclient = zmsg_pop(msg);
		char* operation = zmsg_popstr(msg);
		zframe_t* idworker = getWorkerFor(operation,4);
		if(idworker!=NULL){
			zmsg_pushstr(msg,operation);
			zmsg_prepend(msg, &idclient);
			for(int i=0; i< wr[operation].second.size(); i++){
				cout<<"worker:"<<idworker<<" i:"<<wr[operation].second[i].first<<endl;
				if(zframe_eq(wr[operation].second[i].first, idworker)){
					wr[operation].second[i].second++;
					cout<<"second: "<<wr[operation].second[i].second<<endl;
				}
			}
			zmsg_prepend(msg, &idworker);
			// Prepare and send the message to the worker
			zmsg_send(&msg, workers);

			cout << "End of handling" << endl;
			// zframe_destroy(&clientId);
			free(operation);
			zmsg_destroy(&msg);
			myqueue.pop();
		}
	}
}

void handleWorkerMessage(zmsg_t* msg, void* clients) {
  cout << "Handling the following WORKER" << endl;
  zmsg_print(msg);
  // Retrieve the identity and the operation code
  zframe_t* id = zmsg_pop(msg);
  char* opcode = zmsg_popstr(msg);
  if (strcmp(opcode, "register") == 0) {
    // Get the operation the worker computes
    char* operation = zmsg_popstr(msg);
    // Register the worker in the server state
    registerWorker(id, operation);
    free(operation);
  } else if (strcmp(opcode, "answer") == 0) {
		char *operation = zmsg_popstr(msg);
		for(int i=0; i< wr[operation].second.size(); i++){
			if(zframe_eq (wr[operation].second[i].first, id)){
				wr[operation].second[i].second--;
			}
		}
    zmsg_send(&msg, clients);
  } else {
    cout << "Unhandled message" << endl;
  }
  cout << "End of handling" << endl;
  free(opcode);
  zframe_destroy(&id);
  zmsg_destroy(&msg);
}

int main(void) {
  zctx_t* context = zctx_new();
  // Socket to talk to the workers
  void* workers = zsocket_new(context, ZMQ_ROUTER);
  int workerPort = zsocket_bind(workers, "tcp://*:5555");
  cout << "Listen to workers at: "
       << "localhost:" << workerPort << endl;

  // Socket to talk to the clients
  void* clients = zsocket_new(context, ZMQ_ROUTER);
  int clientPort = zsocket_bind(clients, "tcp://*:4444");
  cout << "Listen to clients at: "
       << "localhost:" << clientPort << endl;

  zmq_pollitem_t items[] = {{workers, 0, ZMQ_POLLIN, 0},
                            {clients, 0, ZMQ_POLLIN, 0}};
  cout << "Listening!" << endl;

  while (true) {
    zmq_poll(items, 2, 10 * ZMQ_POLL_MSEC);
		queueManage(workers);		
    if (items[0].revents & ZMQ_POLLIN) {
      cerr << "From workers\n";
      zmsg_t* msg = zmsg_recv(workers);
      handleWorkerMessage(msg, clients);
    }
    if (items[1].revents & ZMQ_POLLIN) {
      cerr << "From clients\n";
      zmsg_t* msg = zmsg_recv(clients);
			myqueue.push(msg);
      //handleClientMessage(msg, workers);
    }
  }
  // TODO: Destroy the identities
  zctx_destroy(&context);
  return 0;
}
