// client for bank
// ivan dario valencia
//

#include <czmq.h>
#include <string>
#include <iostream>

using namespace std;

int account=0;

void menu(void * requester){
	int op=1,op2=0,x,y;
	zmsg_t* resp;
	zmsg_t* request;
	string msg;
	while(op!=0){
		cout<<endl<<"1- login"<<endl<<"2- crete account"<<endl<<"0- exit"<<endl<<"->: ";
		cin>>op;
		switch(op){
		case 1:
			cout<<"account number?: ";
			cin>>account;
			request = zmsg_new();
			zmsg_addstr(request,"Login");
			zmsg_addstr(request,to_string(account).c_str());
			zmsg_send(&request,requester);
			resp = zmsg_recv(requester);
			zmsg_print(resp);
			msg = zmsg_popstr(resp);
			//cout<<"resp: "<<msg<<endl;
			if(msg=="OK")op2=1;

			while(op2!=0){
				request = zmsg_new();
				cout<<endl<<"1- Deposit"<<endl<<"2- Consult"<<endl<<"3- Withdraw"<<endl<<"4- Transfer"<<endl<<"0- exit"<<endl<<"->: ";
				cin>>op2;
				switch(op2){
				case 1:
					cout<<"deposit balance: ";
					cin>>x;
					zmsg_addstr(request,"Deposit");
					//cout<<to_string(account).c_str()<<endl;
					zmsg_addstr(request,to_string(account).c_str());
					zmsg_addstr(request,to_string(x).c_str());
					break;
				case 2:
					zmsg_addstr(request,"Consult");
					zmsg_addstr(request,to_string(account).c_str());
					//cout<<"consult "<<to_string(account).c_str();
					break;
				case 3:
					cout<<"withdrawing value: "<<endl;
					cin>>x;
					zmsg_addstr(request,"Withdraw");
					zmsg_addstr(request,to_string(account).c_str());
					zmsg_addstr(request,to_string(x).c_str());
					break;
				case 4:
					cout<<"account to transfer: ";
					cin>>x;
					cout<<"value to transfer: ";
					cin>>y;
					zmsg_addstr(request,"Transfer");
					zmsg_addstr(request,to_string(account).c_str());
					zmsg_addstr(request,to_string(y).c_str());
					zmsg_addstr(request,to_string(x).c_str());
					break;
				}
				zmsg_send(&request,requester);
				resp = zmsg_recv(requester);
				cout<<endl;
				zmsg_print(resp);
				cout<<"account number: "<<account;
			}
			break;
		case 2:
			request = zmsg_new();
			zmsg_addstr(request,"Create");
			zmsg_send(&request,requester);
			resp = zmsg_recv(requester);
			zmsg_print(resp);
			break;
		}
	}
}

int main(int argc, char** argv) {
  zctx_t *context = zctx_new();
  void *requester = zsocket_new(context, ZMQ_REQ);
  zsocket_connect(requester, "tcp://localhost:5555");
  // Creates an empty message
  //zmsg_t* request = zmsg_new();
  menu(requester);        
  zctx_destroy(&context);
 
  return 0;
}
