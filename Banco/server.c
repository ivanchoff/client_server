//  server for banc 
//  Ivan Dario Valencia h    
//
#include <czmq.h>
#include <iostream>
#include <unordered_map>
#include <string>

using namespace std;

typedef unordered_map<int,int> account;

// Server's state
account accounts;
int cont=1000;

void dispatch(zmsg_t *incmsg, zmsg_t *outmsg) {
  int cuenta,valor,cuenta2;
  string action  =  zmsg_popstr(incmsg);
	char str[100];

  if(action == "Create"){
    cont++;
    accounts[cont] = 0;
		strcpy(str,"NEW account number: ");
    zmsg_addstr(outmsg, strcat(str,to_string(cont).c_str()));
  }else if(action== "Deposit"){
		cuenta = stoi(zmsg_popstr(incmsg));
	  valor = stoi(zmsg_popstr(incmsg));
    accounts[cuenta]+=valor;
    zmsg_addstr(outmsg, "Deposit OK!!"); 
  }else if(action== "Consult"){
    cuenta = stoi(zmsg_popstr(incmsg));
    if(accounts.count(cuenta)>0){
			strcpy(str,"Account balance: ");
      zmsg_addstr(outmsg,strcat(str, to_string(accounts[cuenta]).c_str()));
    }else{
      zmsg_addstr(outmsg, "Account doesn't exist");
    }
  }else if(action== "Withdraw"){
    cuenta = stoi(zmsg_popstr(incmsg));
    valor = stoi(zmsg_popstr(incmsg));
    if(valor > accounts[cuenta]){
      zmsg_addstr(outmsg, "insufficient balance");
    } 
    else{
      accounts[cuenta]-=valor;
      zmsg_addstr(outmsg, "OK!!"); 
    }
  }else if(action== "Transfer"){
    cuenta = stoi(zmsg_popstr(incmsg));
    valor = stoi(zmsg_popstr(incmsg));
    cuenta2 = stoi(zmsg_popstr(incmsg));
		if(accounts.count(cuenta) && accounts.count(cuenta2)){
			if(valor>accounts[cuenta]){
				zmsg_addstr(outmsg, "insufficient balance");
			}else{
				accounts[cuenta]-=valor;
				accounts[cuenta2]+=valor;
				zmsg_addstr(outmsg, " Transfering OK!!"); 
			}
		}else zmsg_addstr(outmsg, "invalid account number");
	}else if(action=="Login"){
		cuenta = stoi(zmsg_popstr(incmsg));
		if(accounts.count(cuenta)) zmsg_addstr(outmsg, "OK");
		else zmsg_addstr(outmsg, "Account doesn't exist");
	}
}  
int main (void){
  zctx_t *context = zctx_new();
  void *responder = zsocket_new(context, ZMQ_REP);
  zsocket_bind(responder, "tcp://*:5555");
  
  while (1) {
    zmsg_t* request = zmsg_recv(responder);
    zmsg_t *response = zmsg_new();
    dispatch(request,response);
    zmsg_send(&response,responder);
	}
	zctx_destroy(&context);
	return 0;
}


