#include<bits/stdc++.h>
#include<czmq.h>
using namespace std;

int main(int argc, char** argv) {
  zctx_t* context = zctx_new();
  void* left = zsocket_new(context,ZMQ_DEALER);
	void* rigth = zsocket_new(context,ZMQ_DEALER);
	string leader, init, idr; 

	if (argc > 1) {
		idr = argv[1];
		zsocket_connect(left, argv[2]);
		zsocket_bind(rigth,argv[3]);
		init = argv[4];
  }else{
		cout<<"error";
	}
	if(init=="1"){
		leader = idr;
		zmsg_t *msg= zmsg_new();
		zmsg_pushstr(msg,leader.c_str());
		zmsg_send(&msg,left);
		zmsg_destroy(&msg);
	}
	

  zmq_pollitem_t items[] = {{rigth, 0, ZMQ_POLLIN, 0}};

  while(true) {
    zmq_poll(items,1,10*ZMQ_POLL_MSEC);
    if(items[0].revents & ZMQ_POLLIN) {
      zmsg_t *msg = zmsg_recv(rigth);
			zmsg_print(msg);
			idr = zmsg_popstr(msg);

			if(leader==idr){
				zmsg_pushstr(msg,leader.c_str());
				zmsg_send(&msg,left);
				break;
			}
			leader = max(leader,idr);
			zmsg_pushstr(msg,leader.c_str());
			zmsg_send(&msg,left);
      zmsg_destroy(&msg);      
    }
  }
  zctx_destroy(&context);
  return 0;
}
