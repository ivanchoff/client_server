#!/bin/bash

#calcuate sha1sum value of each file and split files
cd files
rm *[^.ogg]
cat /dev/null > SHA1SUMS
cat /dev/null > catalogo
for i in *.ogg;
do
    sha1=$(sha1sum $i)
    set -- $sha1
    name=$1
    echo $sha1 >> SHA1SUMS;
    n_parts=$(split -b 512k --numeric-suffixes=1 --verbose $i $name | wc -l )
    echo $i $name $n_parts >> catalogo
    echo $n_parts > $name.parts
done
