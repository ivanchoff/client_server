//nodo de la red p2p
//choff@riseup.net
#include<bits/stdc++.h>
#include<SFML/Audio.hpp>
#include<czmq.h>
#include<unistd.h>
#include<mutex>
using namespace std;
int quit=0,delay=0,ctrl=1;
mutex mut;
struct song{
  string name;
  bool seed=false;
  int parts;
  vector<string>myparts;
  unordered_map<string,vector<string> > nodes;
};
unordered_map<int,pair<char*,char*>> search_options;
unordered_map<string,song> files; //{hash,song}
queue<string> playlist;
string connected_to;
string own_ip;
void init(string,void*);
void playSongs();
void download_song(string,void*);
void load_struct();
void complete_info_song(string, void*);
void get_song_info(char* ,void*);
void print_options();
void search(string,void*,void*);
void menu(void*, void*);
void handle_rec_msg(zmsg_t*, void*,void*,string);
void handle_send_msg(zmsg_t*, void*, void*);
void control_send(void*);
void control_rec(void*, void*,  string);
void handle_msgs(int, char**, void*, void*);
void list_song();
void send_catalog(string,void*);
void ping(void* s);
//./node id tcp://own_ip:socketReceive tcp://ip_tracker:socket delay
int main(int argc, char** argv){
  //system("cat banner");
  zctx_t* context = zctx_new();
  void* receive = zsocket_new(context,ZMQ_ROUTER);
  void* send = zsocket_new(context, ZMQ_DEALER);
  thread t1(handle_msgs,argc,argv,receive,send);
  menu(send,receive);
  t1.join();
  zmq_close(&receive);
  zmq_close(&send);
  zctx_destroy(&context);
  return 0;
}
void init(string myip, void* send){
  list_song();
  load_struct();
  send_catalog(myip,send);
}
//por estraña razón esta funcion en un hilo no corre
void playSong(){  
  sf::Music music;
  mut.lock();
  int q=quit;
  mut.unlock();
  while(!q){
    //cout<<"mk"<<endl;
    mut.lock();
    int s = playlist.size();
    mut.unlock();
    if(s>0 && music.getStatus() == sf::Music::Stopped){
      //cout<<"playlist.size(): "<<s<<endl;
      mut.lock();
      string front = playlist.front();
      mut.unlock();
      if (!music.openFromFile("./files/" + front))
        cout<<"error  no puedo leer el archivo:"<<front<<endl;
  
      music.play();
      mut.lock();
      playlist.pop();
      mut.unlock();
    }
  }
}
void load_struct(){
  //cout<<"loading struct files"<<endl;
  ifstream file("./files/catalogo");
  if(file ){
    stringstream buffer;
    buffer << file.rdbuf();
    file.close();
    string name,hash,tam;
    while(buffer>>name>>hash>>tam){
      song x;
      x.name = name;
      x.seed = true;
      x.parts = stoi(tam);
      for(int i=1; i<=x.parts; i++){
        string tmp="";
        if(i<10)tmp="0";
        tmp+=to_string(i);
        x.myparts.push_back(tmp.c_str());
      }
      files[hash] = x;
    }
  }  
}
void get_song_info(char* hash, void* send){
  zmsg_t* msg = zmsg_new();
  zmsg_addstr(msg,"info_song");
  zmsg_addstr(msg,hash);
  //zmsg_print(msg);
  zmsg_send(&msg,send);
}
void print_options(){
  cout<<"Imprimo search_options"<<endl;
  for(auto it=search_options.begin(); it != search_options.end(); ++it){
    cout<<it->first<<" - "<<it->second.first<<endl;
  }
}
void search(string songname,void* send, void* rec){
  zmsg_t* msg = zmsg_new();
  zmsg_addstr(msg,"search");
  zmsg_addstr(msg,songname.c_str());
  //zmsg_print(msg);
  zmsg_send(&msg,send);
}
void menu(void* send, void* rec){
  int op=1;
  string cad;
  while(op!=0 && ctrl){
    cout<<endl<<"************************************"<<endl;
    cout<<"******** FreeSound Options:  *******"<<endl;
    cout<<"1 - Search"<<endl;
    cout<<"2 - View List"<<endl;
    cout<<"3 - play list"<<endl;
    cout<<"4 - actualizar catalogo"<<endl;
    cout<<"0 - Quit\n opcion: "<<endl;
    cin>>op;
    if(op==1){ // search
      string cad;
      cout<<"ingrese nombre:";
      cin>>cad;
      search(cad,send,rec);
      op=0;
    }
    if(op==2){ //view list
      //printSongList();
    }
    if(op==3){ //play list
      //playlst=1;
      //playList();
    }
  }
}
void handle_rec_msg(zmsg_t* msg, void* rec, void* send, string tracker){
  //cout<<"recibiendo esto del peer"<<endl;
  // zmsg_print(msg);
  char* id = zmsg_popstr(msg);
  char* op = zmsg_popstr(msg);

  if(strcmp(op,"partsInfo")==0){
    string ip = zmsg_popstr(msg);
    string hash = zmsg_popstr(msg);
    song x = files[hash];
    int d =zsocket_disconnect(send, connected_to.c_str());
    //cout<<"Disconnect "<<((d==0)? "OK":"Error")<<endl;
    int c =zsocket_connect(send, ip.c_str());
    cout<<"connecting to "<<ip<<((c==0)? " OK":" Error")<<endl;
    if(c==0){
     connected_to = ip;
     zmsg_t* msg2 = zmsg_new();
     //zmsg_addstr(msg2,id);
     zmsg_addstr(msg2,"partInfoAnswer");
     zmsg_addstr(msg2,own_ip.c_str());
     zmsg_addstr(msg2,hash.c_str());
     for(int i=0; i<x.myparts.size(); i++)
       zmsg_addstr(msg2,x.myparts[i].c_str());
     zmsg_addstr(msg2,"end");
     //zmsg_print(msg2);
     zmsg_send(&msg2,send);
    }
  }
  if(strcmp(op,"partInfoAnswer")==0){
    string ip = zmsg_popstr(msg);
    string hash =  zmsg_popstr(msg);
    string part = zmsg_popstr(msg);
    while(strcmp(part.c_str(),"end")!=0){
      files[hash].nodes[ip].push_back(part);
      part = zmsg_popstr(msg);
    }
    if(!files[hash].seed){
      string partx = "";
      bool flag = true;
      //busca una parte que no tenga para pedirla
      for(int i=0; i < files[hash].nodes[ip].size(); i++ ){
        partx = files[hash].nodes[ip][i];
        flag=true;
        for(int j=0; j<files[hash].myparts.size(); j++){
          if(partx == files[hash].myparts[j]){
            flag = false;
            break;
          }
        }
      }
      if(flag){
        int d =zsocket_disconnect(send, connected_to.c_str());
        //cout<<"Disconnect "<<((d==0)? "OK":"Error")<<endl;
        int c =zsocket_connect(send, ip.c_str());
        //cout<<"connecting to "<<ip<<((c==0)? " OK":" Error")<<endl;
        if(c==0){
          connected_to = ip;
          zmsg_t* msg2 = zmsg_new();
          zmsg_addstr(msg2,"getPart");
          zmsg_addstr(msg2,own_ip.c_str());
          zmsg_addstr(msg2,hash.c_str());
          zmsg_addstr(msg2,partx.c_str());
          cout<<"solicito parte "<<partx<<" a "<<ip<<endl;
          //zmsg_print(msg2);
          zmsg_send(&msg2,send);
        }
      }
    }
  }
  if(strcmp(op,"getPart")==0){
    string ip =  zmsg_popstr(msg);
    string hash = zmsg_popstr(msg);
    string part = zmsg_popstr(msg);
    string partx = "./files/";
    partx += hash;
    partx += part;
    zchunk_t *chunk = zchunk_slurp(partx.c_str(),0);
    if(!chunk){
      cout << "Cannot read file! "<<partx<<endl;
      return;
    }
    //cout << "Chunk size: " << zchunk_size(chunk) << endl;
    zframe_t *frame=zframe_new(zchunk_data(chunk), zchunk_size(chunk));
    int d =zsocket_disconnect(send, connected_to.c_str());
    //cout<<"Disconnect "<<((d==0)? "OK":"Error")<<endl;
    int c =zsocket_connect(send, ip.c_str());
    //cout<<"connecting to "<<ip<<((c==0)? " OK":" Error")<<endl;
    if(c==0){
      //envia parte al peer
      connected_to = ip;
      //mensaje de prueba
      zmsg_t* res2 = zmsg_new();
      zmsg_addstr(res2,"ping");
      //cout<<"sendind ping"<<endl;
      //zmsg_print(res2);
      zmsg_send(&res2,send);
      
      zmsg_t* res = zmsg_new();
      zmsg_addstr(res,"getPartAnswer");
      zmsg_addstr(res,own_ip.c_str());
      zmsg_addstr(res,hash.c_str());
      zmsg_addstr(res,part.c_str());
      zmsg_append(res,&frame);
      cout<<"sendind part to peer"<<endl;
      //zmsg_print(res);
      zmsg_send(&res,send);
     }
  }
  if(strcmp(op,"getPartAnswer")==0){
    //save part
    string ip = zmsg_popstr(msg);
    string hash = zmsg_popstr(msg);
    string part = zmsg_popstr(msg);
    string partx = hash;
    cout<<"recivo parte "<<part<<" del nodo "<<id<<endl;
    partx += part;
    zframe_t *file = zmsg_pop(msg);
    string old="./data/music/received/";
    zfile_t* download = zfile_new("./files/",partx.c_str());
    zfile_output(download);
    zchunk_t* chunk = zchunk_new(zframe_data(file),zframe_size(file));
    //cout << "Chunk size: " << zchunk_size(chunk) << endl;
    //cout<<"File is readable? "<< zfile_digest(download) << endl;
    zfile_write(download,chunk,0);
    zfile_close(download);
    //struct update
    files[hash].myparts.push_back(part);
    if(files[hash].myparts.size()==files[hash].parts)
      files[hash].seed = true;
    if(!files[hash].seed){
      string party = "";
      bool flag = true;
      for(int i=0; i < files[hash].nodes[ip].size(); i++ ){
        party = files[hash].nodes[ip][i];
        flag=true;
        for(int j=0; j<files[hash].myparts.size(); j++){
          if(party == files[hash].myparts[j]){
            flag = false;
            break;
          }
        }
        if(flag){
          break;
        }
      }
      //cout<<"entro por aca flag="<<flag<<endl;
      if(flag){
        int d =zsocket_disconnect(send, connected_to.c_str());
        //cout<<"Disconnect "<<((d==0)? "OK":"Error")<<endl;
        int c =zsocket_connect(send, ip.c_str());
        //cout<<"connecting to "<<ip<<((c==0)? " OK":" Error")<<endl;
        if(c==0){
          connected_to = ip;
          zmsg_t* msg2 = zmsg_new();
          zmsg_addstr(msg2,"getPart");
          zmsg_addstr(msg2,own_ip.c_str());
          zmsg_addstr(msg2,hash.c_str());
          zmsg_addstr(msg2,party.c_str());
          cout<<"solicito parte "<<party<<" al nodo "<<ip<<endl;
          //zmsg_print(msg2);
          zmsg_send(&msg2,send);
        }
      }
    }
    else{
      //soy seed
      string songname = files[hash].name;
      string cmd = "cat ./files/";
      cmd += hash;
      cmd += "* > ./files/";
      cmd += songname;
      //join parts
      system(cmd.c_str());
    
      
      cout<<"\n-----------------------"<<endl;
      cout<<"| playing -> "<<songname<<endl;
      cout<<"-----------------------"<<endl;
      
      sf::Music music;
      if (!music.openFromFile("./files/" + songname))
        cout<<"error  no puedo leer el archivo:"<<songname<<endl;
      music.play();
      while (music.getStatus() == sf::Music::Playing){
        // Leave some CPU time for other processes
        sf::sleep(sf::milliseconds(100));
        // Display the playing position
        std::cout << "\rPlaying... " << std::fixed << std::setprecision(2) << music.getPlayingOffset().asSeconds() << " sec   ";
        std::cout << std::flush;
      }

      //registrarme en el tracker
      int d =zsocket_disconnect(send, connected_to.c_str());
      //cout<<"Disconnect "<<((d==0)? "OK":"Error")<<endl;
      int c =zsocket_connect(send, tracker.c_str());
      //cout<<"connecting to "<<ip<<((c==0)? " OK":" Error")<<endl;
      if(c==0){
        connected_to = tracker;
        zmsg_t* msg2 = zmsg_new();
        zmsg_addstr(msg2,"newSeed");
        zmsg_addstr(msg2,own_ip.c_str());
        zmsg_addstr(msg2,hash.c_str());
        //zmsg_print(msg2);
        zmsg_send(&msg2,send);
      }
      menu(send,rec);
    }
  }
  if(strcmp(op,"ping")==0){
    zmsg_t* res = zmsg_new();
    zmsg_addstr(res,id);
    zmsg_addstr(res,"pong");
    zmsg_send(&res,rec);    
  }
  if(strcmp(op,"pong")==0){
    //cout<<"conexion establecida"<<endl;
  }
  zmsg_destroy(&msg);
}
void handle_send_msg(zmsg_t* msg, void* send,void* rec){
  cout<<"recibiendo esto del tracker o peer"<<endl;
  //zmsg_print(msg);
  char* op = zmsg_popstr(msg);

  if(strcmp(op,"searchResult")==0){
    int i=0;
    search_options.clear();
    char* name = zmsg_popstr(msg);
    while(strcmp(name,"end") != 0){
      char* hash = zmsg_popstr(msg);
      //cout<<hash<<endl;
      search_options[i++] = make_pair(name,hash);
      name = zmsg_popstr(msg);
    }
    //system("clear");
    print_options();
    int op2;
    cout<<"option:";
    cin>>op2;
    //cout<<"search_op"<<search_options[op2].first;
    if(op2<search_options.size() && op2>=0){
      char* hash = search_options[op2].second;
      cout<<hash<<endl;
      get_song_info(hash,send);
    }else{
      cout<<"invalid option"<<endl;
      menu(send,rec);
    }
  }
  if(strcmp(op,"info_song")==0){
    string hash = zmsg_popstr(msg);
    string name = zmsg_popstr(msg);
    int parts = stoi(zmsg_popstr(msg));
    string node = zmsg_popstr(msg);
    song x;
    x.name = name;
    x.parts = parts;
    vector<string>myparts;
    x.myparts = myparts;
    x.seed = false;
    while(strcmp(node.c_str(),"end") != 0){
      x.nodes[node] = myparts;
       node = zmsg_popstr(msg);
    }
    files[hash] = x;
    complete_info_song(hash,send);
  }
  
}
void complete_info_song(string hash,void* send){
  song tmp = files[hash];
  for(auto it = tmp.nodes.begin(); it!= tmp.nodes.end(); ++it){
    string to_ip = it->first;
    int d =zsocket_disconnect(send, connected_to.c_str());
    //cout<<"Disconnect "<<((d==0)? "OK":"Error")<<endl;
    int c =zsocket_connect(send, to_ip.c_str());
    cout<<"connecting to "<<to_ip<<((c==0)? " OK":" Error")<<endl;
    if(c==0){
      connected_to = to_ip;
      zmsg_t* msg = zmsg_new();
      zmsg_addstr(msg,"partsInfo");
      zmsg_addstr(msg,own_ip.c_str());
      zmsg_addstr(msg,hash.c_str());
      //zmsg_print(msg);
      zmsg_send(&msg,send);
    }
  }
}

void handle_msgs(int argc, char **argv,void* rec, void* send){
  string ip_tracker;
	if (argc > 1) {
		char* identity = argv[1];
    own_ip = argv[2];
    ip_tracker = argv[3];
    delay = stoi(argv[4]);
    zsocket_set_identity(rec,identity);
    zsocket_set_identity(send,identity);
  }

  int s = zsocket_connect(send,ip_tracker.c_str());
  cout << "connecting to tracker: " << (s == 0 ? "OK" : "ERROR") << endl;
  connected_to = ip_tracker;
  int b = zsocket_bind(rec,own_ip.c_str());
  cout<<"bindin at "<<own_ip<<(b != 0 ? " ok" : " error")<<endl;

  init(own_ip,send);
  
  zmq_pollitem_t items[] = {{rec, 0, ZMQ_POLLIN, 0},
                            {send, 0, ZMQ_POLLIN, 0}};
 
  while (!quit) {
    zmq_poll(items, 2, 10 * ZMQ_POLL_MSEC);
      if (items[0].revents & ZMQ_POLLIN) {
        zmsg_t* msg = zmsg_recv(rec);
        //cout<<"ok receive"<<endl;
        sleep(delay);
        handle_rec_msg(msg, rec, send,ip_tracker);
      }
      if (items[1].revents & ZMQ_POLLIN) {
        zmsg_t* msg = zmsg_recv(send);
        sleep(delay);
        handle_send_msg(msg, send,rec);
    }
  }
}
void list_song(){
  system("./lister.sh");
}
void send_catalog(string myip, void* send){
  //cout<<"send catalog"<<endl;
  ifstream file("./files/catalogo");
  if(file ){
    stringstream buffer;
    buffer << file.rdbuf();
    file.close();
    string name,hash,tam;
    zmsg_t* msg = zmsg_new();
    zmsg_addstr(msg,"register");
    zmsg_addstr(msg, myip.c_str());
    while(buffer>>name>>hash>>tam){
      cout<<name<<endl;
      zmsg_addstr(msg,name.c_str());
      zmsg_addstr(msg,hash.c_str());
      zmsg_addstr(msg,tam.c_str());
    }
    zmsg_addstr(msg,"end");
    zmsg_send(&msg,send);
  }
}
void ping(void* socket){
  zmsg_t* msg = zmsg_new();
  //zmsg_addstr(msg,"tracker");
  zmsg_addstr(msg,"ping");
  //zmsg_print(msg);
  zmsg_send(&msg,socket);
}
