//tracker de la red p2p
#include<bits/stdc++.h>
#include<czmq.h>
using namespace std;
struct song{
  char* name;
  char* parts;
  vector<string> nodes;
};
//mapa{(song_name, sha1sum)}
unordered_map<char*, string> songs_list;
unordered_map<string, song> catalog;        //{sha1sum, object(song)}
void print_catalog();
void print(vector<char*>);
void handle_node_msg(zmsg_t*, void* );
//./tracker id tcp://127.0.0.1:5557
int main(int argc, char** argv) {
  cout << "tracker agent  starting..." << endl;
  zctx_t* context = zctx_new();
  void* receive = zsocket_new(context, ZMQ_ROUTER);   //to broker
  void* send = zsocket_new(context, ZMQ_ROUTER);   //To worker
  string own_ip;
  if (argc > 1) {
		char* identity = argv[1];
    own_ip = argv[2];
    zsocket_set_identity(receive,identity);
    zsocket_set_identity(send,identity);
  }
  int b = zsocket_bind(receive,own_ip.c_str());
  cout<<"bindin at "<<argv[2]<<(b == 5557 ? " ok" : " error")<<endl;
  zsocket_bind(send,"tcp://127.0.0.1:5558");

  while (1) {
    zmq_pollitem_t items[] = {{receive, 0, ZMQ_POLLIN, 0},
                              {send, 0, ZMQ_POLLIN, 0}};
  
    zmq_poll(items, 2, 10 * ZMQ_POLL_MSEC);
    if (items[0].revents & ZMQ_POLLIN) {
      zmsg_t* msg = zmsg_recv(receive);
      handle_node_msg(msg, receive);
    }
    if (items[1].revents & ZMQ_POLLIN) {
      zmsg_t* msg = zmsg_recv(send);
      zmsg_print(msg);
      //handleWorkerMsg(msg, send);
    }
  }
  zctx_destroy(&context);
  return 0;
}

void print_catalog(){
  cout<<"imprimo catalogo"<<endl;
  for(auto it=catalog.begin(); it != catalog.end(); ++it){
    cout<<it->first<<" : "<<it->second.name<<endl;
  }
}
void handle_node_msg(zmsg_t* msg, void* rec){
  cout<<"recibiendo esto del nodo"<<endl;
  zmsg_print(msg);
  char* id = zframe_strdup(zmsg_pop(msg));
  char* op = zmsg_popstr(msg);

  if(strcmp(op,"register")==0){
    string ip = zmsg_popstr(msg);
    char* name = zmsg_popstr(msg);
    while(strcmp(name,"end")!=0){
      string sha1 = zmsg_popstr(msg);
      char* parts = zmsg_popstr(msg);
      songs_list[name] = sha1;
      song tmp;
      tmp.name = name;
      tmp.parts = parts;
      tmp.nodes.push_back(ip);
      catalog[sha1] = tmp;
      //cout<<"catalogo[sha1].name:"<<catalog[sha1].name<<endl;
      name = zmsg_popstr(msg);
    }
    zmsg_addstr(msg,id);
    zmsg_addstr(msg,op);
    zmsg_print(msg);
    //zmsg_send(&msg,rec);
  }
  if(strcmp(op,"search")==0){
    char* songname = zmsg_popstr(msg);
    zmsg_t* msg2 = zmsg_new();
    zmsg_addstr(msg2,id);
    zmsg_addstr(msg2,"searchResult");
    for(auto it = songs_list.begin(); it != songs_list.end(); ++it){
      string key = it->first;
      string hash = it->second;
      if(key.find(songname) != string::npos){
        zmsg_addstr(msg2,key.c_str());
        zmsg_addstr(msg2,hash.c_str());
      }
    }
    zmsg_addstr(msg2,"end");
    zmsg_send(&msg2,rec);
  }
  if(strcmp(op,"info_song")==0){
    //print_catalog();
    string hash = zmsg_popstr(msg);
    cout<<"hash: "<<hash<<endl;
    zmsg_t* msg2 = zmsg_new();
    song x = catalog[hash];
    zmsg_addstr(msg2,id);
    zmsg_addstr(msg2,"info_song");
    zmsg_addstr(msg2,hash.c_str());
    zmsg_addstr(msg2,x.name);
    zmsg_addstr(msg2,x.parts);
    for(auto &it : x.nodes ){
      zmsg_addstr(msg2,it.c_str());
    }
    zmsg_addstr(msg2,"end");
    zmsg_send(&msg2,rec);
  }
  if(strcmp(op,"newSeed")==0){
    string ip = zmsg_popstr(msg);
    string hash = zmsg_popstr(msg);
    catalog[hash].nodes.push_back(ip);
  }
}

void print(vector<char*> x){
  for(auto i: x){
    cout<<i<<endl;
  }
}
