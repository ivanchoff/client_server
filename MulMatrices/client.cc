#include<bits/stdc++.h>
#include<czmq.h>

// mensaje al server
// ________________________________________________________
//| op |filasA | colA | matrizA | filasB | colB | Matriz B |
// --------------------------------------------------------

using namespace std;

int main(int argc, char** argv) {
  zctx_t* context = zctx_new();
  void* client = zsocket_new(context,ZMQ_DEALER);
	string row, col, tmp,matriz,identity;
	stringstream ss;

  cin>>identity;
  zsocket_set_identity(client,identity.c_str());
 
  zsocket_connect(client, "tcp://localhost:5555");
	
	zmsg_t *msg= zmsg_new();	
	zmsg_addstr(msg,"Mult");
  cin>>row>>col;
	zmsg_addstr(msg, row.c_str());
	zmsg_addstr(msg, col.c_str());
  matriz="";
  cout<<"---> A:"<<endl;
  for(int i=0; i<stoi(row); i++){       //Matriz A
		for(int j=0; j<stoi(col); j++){
			cin>>tmp;
      cout<<tmp<<" ";
      matriz+=tmp+" ";
		}
    cout<<endl;
  }
  zmsg_addstr(msg, matriz.c_str());
  matriz="";
	cin>>row>>col;
	zmsg_addstr(msg, row.c_str());
	zmsg_addstr(msg, col.c_str());
  cout<<"---> B: "<<endl;
	for(int i=0; i<stoi(row); i++){
		for(int j=0; j<stoi(col); j++){
			cin>>tmp;
      cout<<tmp<<" ";
      matriz+=tmp+" ";
    }
    cout<<endl;
  }


  zmsg_addstr(msg, matriz.c_str());
	zmsg_send(&msg,client);
	msg = zmsg_recv(client);
	cout<<endl<<"A x B :"<<endl;
  
  int r = stoi(zmsg_popstr(msg));
  int c = stoi(zmsg_popstr(msg));
  ss<< zmsg_popstr(msg);
	for(int i=0; i<r; i++){
		for(int j=0; j<c; j++){
      ss>>tmp;
      cout<<tmp+" ";
    }
    cout<<endl;
  }
	zmsg_destroy(&msg);
  zctx_destroy(&context);
  return 0;
}
