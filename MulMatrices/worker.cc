// worker para el multiplicador de matrices...
//  choff@riseup.net

#include<bits/stdc++.h>
#include <czmq.h>

using namespace std;

// ____________________________________
//| idCliente | op | i | rowsize | row |
// ------------------------------------           
void handleServerMessage(zmsg_t* msg, void* server) {
  cout << "Handling the following from SERVER" << endl;
  zmsg_print(msg);
 
  stringstream ss;
  zframe_t* idc = zframe_dup(zmsg_pop(msg));
	char * op = zmsg_popstr(msg);
	char * ind = zmsg_popstr(msg);	
	int rowA = stoi(zmsg_popstr(msg));
	int fila[rowA];
  ss << zmsg_popstr(msg);
  for(int i=0; i<rowA; i++)ss>>fila[i];
	int rowB = stoi(zmsg_popstr(msg));
	int colB = stoi(zmsg_popstr(msg));
	int B[rowB][colB];
	string resp;
  ss << zmsg_popstr(msg);
	for(int i=0; i< rowB; i++)
		for(int j=0; j< colB; j++)
			ss>>B[i][j];

	for(int k=0; k<colB; k++){
		int cont=0;
		for(int l=0; l<rowA; l++ ){
			cont+=fila[l]*B[l][k];
		}
		resp+=to_string(cont)+" ";
	}

  zmsg_t* result = zmsg_new();
  zmsg_addstr(result, "answer");                // op
	zmsg_append(result,&idc);                     // idclient
  zmsg_addstr(result,ind);                      // i
	zmsg_addstr(result, to_string(colB).c_str()); // rowlength
	zmsg_addstr(result, resp.c_str());            // response

  cout<<"respondo: "<<endl; 
  zmsg_print(result);
  zmsg_send(&result, server);
  cout << "End of handling" << endl;
  free(op);
  free(ind);
  zmsg_destroy(&msg);
}

int main(int argc, char** argv) {
  cout << "Worker agent starting..." << endl;
  zctx_t* context = zctx_new();
  void* server = zsocket_new(context, ZMQ_DEALER);
	
	if (argc > 1) {
		char *identity = argv[1];
    zsocket_set_identity(server,identity);
  }


  int c = zsocket_connect(server, "tcp://localhost:5556");
  cout << "connecting to server: " << (c == 0 ? "OK" : "ERROR") << endl;

  // Register the worker with the server
  zmsg_t* regmsg = zmsg_new();
  zmsg_addstr(regmsg, "register");
  //zmsg_addstr(regmsg, argv[1]);
  zmsg_send(&regmsg, server);
  

  zmq_pollitem_t items[] = {{server, 0, ZMQ_POLLIN, 0}};

  while (true) {
    zmq_poll(items, 1, 10 * ZMQ_POLL_MSEC);
    if (items[0].revents & ZMQ_POLLIN) {
      zmsg_t* msg = zmsg_recv(server);
      handleServerMessage(msg, server);
    }
  }
  zctx_destroy(&context);
  return 0;
}
