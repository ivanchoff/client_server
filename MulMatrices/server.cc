// servidor para el multiplicador de matrices distribuido.
// choff@riseup.net

#include<bits/stdc++.h>
#include<czmq.h>

using namespace std;

struct dataClient{
	int control,rows,cols;
	map<int,string> res;
};

typedef unordered_map<string, dataClient > clientsRef;

clientsRef clients;
vector<zframe_t*>  workers;

int indice=0;

void print(const dataClient& d){
  cout<<"rowssss: "<<d.control<<endl;
}

zframe_t* getWorkerFor() {
	zframe_t* wid;
	wid = zframe_dup(workers[indice]);
	(indice == workers.size()-1)? indice=0 : indice++;
	return wid;
}

//mensaje al worker
// ___________________________________________________________
//| idcli | op | i | rowSize | rowA | rowBsize | colBsize | B |
// -----------------------------------------------------------

void handleClientMessage(zmsg_t* msg, void* worker){
	cout<<"manejando esto del cliente"<<endl;
  stringstream ss;
  zmsg_print(msg);
	zframe_t* idclient = zmsg_pop(msg);
  string idc= zframe_strdup(idclient);
	char* op = zmsg_popstr(msg);
	vector<string> A;
	string B;
	map<int,string> C;

	if(strcmp(op,"Mult")==0){
    dataClient client;   // struct
    clients[idc]=client;
    string tmp,num;
		int row= stoi(zmsg_popstr(msg));
		int col= stoi(zmsg_popstr(msg));
    ss << zmsg_popstr(msg);             
    clients[idc].control=row; 
    clients[idc].rows=row;   
    clients[idc].res=C;
		for(int i=0; i<row; i++){  // llena la matriz A.
      tmp="";
			for(int j=0; j<col; j++){
        ss>>num;
        tmp+=num+" ";
      }
      A.push_back(tmp);
		}
    
		row= stoi(zmsg_popstr(msg));
		col= stoi(zmsg_popstr(msg)); 
    clients[idc].cols=col;
    B = zmsg_popstr(msg);             
    
    for(int i=0; i< clients[idc].rows; i++){  //distribuir mult
      zframe_t* idworker = getWorkerFor();
      zmsg_t* toworker =  zmsg_new();    
			zmsg_addstr(toworker,idc.c_str());            //idclient
			zmsg_addstr(toworker,op);                     //op
			zmsg_addstr(toworker,to_string(i).c_str());   //i
			zmsg_addstr(toworker,to_string(row).c_str()); //row length 
      zmsg_addstr(toworker,A[i].c_str());           //file of A
			zmsg_addstr(toworker,to_string(row).c_str()); //Brow length
			zmsg_addstr(toworker,to_string(col).c_str()); //Bcol length
      zmsg_addstr(toworker,B.c_str());              //B
			
			zmsg_prepend(toworker, &idworker);
      cout<<"sending this msg to worker"<<endl;
      zmsg_print(toworker);
      
			zmsg_send(&toworker, worker);
      
		}
		zmsg_destroy(&msg);
	}
}

void response(string idc, void* client){
	zmsg_t* msg = zmsg_new();
	zmsg_pushstr(msg,idc.c_str());                          // idclient
  zmsg_addstr(msg,to_string(clients[idc].rows).c_str());  // rows
  zmsg_addstr(msg,to_string(clients[idc].cols).c_str());  // cols 
  string resp;
  for(int i=0; i<clients[idc].res.size(); i++){
    resp += clients[idc].res[i];
  }
  zmsg_addstr(msg, resp.c_str());  // result
  cout<<"enviando respuesta al cliente"<<endl;
  zmsg_print(msg);
	zmsg_send(&msg,client);
	clients.erase(idc);
}

// ________________________________________________ 
//| idworker | op | idclient | i | rowlength | row |
// ------------------------------------------------
void handleWorkerMessage(zmsg_t* msg, void* client){
  cout << "Handling the following WORKER" << endl;
  zmsg_print(msg);
  zframe_t* idw = zmsg_pop(msg);
  char* opcode = zmsg_popstr(msg);
  if (strcmp(opcode, "register") == 0){
    zframe_t* idworker = zframe_dup(idw);
    workers.push_back(idworker);
  } else if (strcmp(opcode, "answer") == 0) {
		zframe_t* idclient = zmsg_pop(msg);
    string idc = zframe_strdup(idclient); 
    int i = stoi(zmsg_popstr(msg));
		int row = stoi(zmsg_popstr(msg));
    string res = zmsg_popstr(msg);
    clients[idc].res[i]=res;  
    clients[idc].control--; 
    cout<<"partes de la matriz resultado"<<endl;
    for(int i=0; i<clients[idc].res.size(); i++)
      cout<<clients[idc].res[i]<<endl;
    if(clients[idc].control == 0) response(idc,client);
  }
  cout << "End of handling" << endl;
  free(opcode);
  zframe_destroy(&idw);
  zmsg_destroy(&msg);
}

int main(){
	zctx_t* context = zctx_new();
	void* client =  zsocket_new(context,ZMQ_ROUTER);
	void* worker =  zsocket_new(context,ZMQ_ROUTER);
	zsocket_bind(client,"tcp://*:5555");
	zsocket_bind(worker,"tcp://*:5556");

	zmq_pollitem_t items[]= { {client, 0, ZMQ_POLLIN, 0},
                            {worker, 0, ZMQ_POLLIN, 0}};
	while(true){
		zmq_poll(items, 2, 10 * ZMQ_POLL_MSEC);		
    if (items[0].revents & ZMQ_POLLIN) {  // msg from clients
      zmsg_t* msg = zmsg_recv(client);
      handleClientMessage(msg,worker);
    }
    if (items[1].revents & ZMQ_POLLIN) {  // msg from workers
      zmsg_t* msg = zmsg_recv(worker);
      handleWorkerMessage(msg,client);
    }
	}
	zctx_destroy(&context);
	return 0;
}
