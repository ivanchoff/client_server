#include<bits/stdc++.h>
#include <czmq.h>

using namespace std;

//implementr un mapa con la identidad como llave y el socket como valor:

unordered_map<string,vector<zframe_t *>> workers;

void workerDispatch(zmsg_t *msg, void *client){

	if(zmsg_size(msg)==2){ //add worker to map;
		//zframe_t *id = zmsg_pop(msg);
		zframe_t *id = zframe_dup (zmsg_pop(msg));
		string op = zmsg_popstr(msg);
		workers[op].push_back(id);
		//cout<<endl<<workers[op][0]<<endl;
		//zframe_destroy(&id);
		//zmsg_t *msg =zmsg_new();
		//zmsg_append(msg,&workers[op][0]);
		//zmsg_addstr(msg,"almacenado");
		//zmsg_send(&msg,s);
		//zmsg_destroy(&msg);
	}else{
	//atender los mensajes del worker
		cout<<"recibido"<<endl;
		zmsg_print(msg);
		zmsg_pop(msg);
		//zframe_t *identity_worker = zmsg_pop(msg);
		//zframe_t *identity_client = zmsg_pop(msg);
		//zframe_t *content = zmsg_pop(msg);
		assert(msg);
		zmsg_send(&msg,client);
		zmsg_destroy(&msg);
		//zframe_send(&identity_client, client, ZFRAME_REUSE + ZFRAME_MORE);
		//zframe_send(&content, client, ZFRAME_REUSE);
		//zframe_destroy(&identity_worker);
		//zframe_destroy(&identity_client);
		//zframe_destroy(&content);
	}
}

void clientDispatch(zmsg_t *msg, void *worker){
	zmsg_t *out = zmsg_new();
	//assert(msg);
	zframe_t *id_client = zmsg_pop(msg);
	string op = zmsg_popstr(msg);
	
	if(workers[op].size()>0){
		zmsg_prepend(out,&workers[op][0]);
		zmsg_append(out,&id_client);
		zmsg_addstr(out,op.c_str());
		while(zmsg_size(msg)>0) {
			zframe_t *tmp = zmsg_pop(msg);
			zmsg_append(out,&tmp);
			zframe_destroy(&tmp);
		}
		zmsg_print(out);
		zmsg_send(&out,worker);
		cout<<"envio al worker"<<endl;
	}
 
	zmsg_destroy(&out);
	zmsg_destroy(&msg);
	//zframe_send(&identity_client, worker, ZFRAME_REUSE + ZFRAME_MORE);
	//zframe_send(&content, worker, ZFRAME_REUSE);

	zframe_destroy(&id_client);
	//zframe_destroy(&content);
}


int main(int argc, char** argv) {
  zctx_t* context = zctx_new();
  void *client = zsocket_new(context,ZMQ_ROUTER);
  void *worker = zsocket_new(context,ZMQ_ROUTER);

	if(argc>1){//set identity to sockets
		char *idc = argv[1];
		char *idw = argv[2];
		zsocket_set_identity(client,idc);
		zsocket_set_identity(worker,idw);
	}
	
	int a=0;
  zsocket_bind(client, "tcp://*:5555");
	zsocket_bind(worker, "tcp://*:5556");

  zmq_pollitem_t items[] = {{client,0,ZMQ_POLLIN,0},{worker, 0, ZMQ_POLLIN, 0}};
	//zmq_pollitem_t items[] = {{client,0,ZMQ_POLLIN,0}};

  while(true) {
    zmq_poll(items,2,10*ZMQ_POLL_MSEC);
		if(items[0].revents & ZMQ_POLLIN){ //something in from clients
				zmsg_t *msg = zmsg_recv(client);
				zmsg_print(msg);
				cout<<"imprimo el mensage del cliente"<<endl;
				cout<<"size: "<<zmsg_size(msg)<<endl;
				//zmsg_send(&msg,client);
				clientDispatch(msg,worker);
				zmsg_destroy(&msg);
		}
		if(items[1].revents & ZMQ_POLLIN){ //something in from workers
			cout<<"holaaaaaaaaaaaaaaaaas"<<endl;
			zmsg_t *msg = zmsg_recv(worker);
			zmsg_print(msg);
			cout<<"imprimo el mensage del worker"<<endl;
			cout<<"size: "<<zmsg_size(msg)<<endl;
			workerDispatch(msg,client);
			zmsg_destroy(&msg);
		}
  }
	zmq_close(client);
	zmq_close(worker);
  zctx_destroy(&context);
  return 0;
}
