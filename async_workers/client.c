// ivanchoff practica clients-broker-workers asynchronuos
#include <iostream>
#include <czmq.h>
#include <string.h>


using namespace std;

int main(int argc, char** argv) {
  zctx_t* context = zctx_new();
  void* client = zsocket_new(context,ZMQ_DEALER);
	if(argc > 1){
		char *id = argv[1];
		zsocket_set_identity(client,id);
	}
	zmsg_t *request;
	zmsg_t *resp;
	int i=1;
  zsocket_connect(client, "tcp://localhost:5555");

  zmq_pollitem_t items[] = {{client, 0, ZMQ_POLLIN, 0}};
 
  while(true) {
    zmq_poll(items,1,10*ZMQ_POLL_MSEC);
    if(items[0].revents & ZMQ_POLLIN) {//something from broker
			cout << "Incomming message:\n";
			zmsg_t *msg = zmsg_recv(client);
			//zmsg_pop(msg);
			zmsg_print(msg);
			zmsg_destroy(&msg);      
    }
		if(i%500==0){
			request = zmsg_new();
			zmsg_addstr(request,"Add");
			zmsg_addstr(request,"8");
			zmsg_addstr(request,"3");
			zmsg_print(request);
			zmsg_send(&request,client);
			i=1;
		}
		i++;
  }
	zmsg_destroy(&request);
	zmsg_destroy(&resp);
	zmq_close(client);
  zctx_destroy(&context);
  return 0;
}
