// ivanchoff practica clients-broker-workers asynchronuos
#include <iostream>
#include <set>
#include <string>
#include <czmq.h>
#include <string.h>

//#define cf ios_base::sync_with_stdio(NULL);cin.tie(false);

using namespace std;

void dispatch(zmsg_t *msg, void *server){
	zmsg_print(msg);
	cout<<"recibo del manager"<<endl;
  //zmsg_pop(msg);
	/*zframe_t *identity = zmsg_pop(msg);
	char *operation = zmsg_popstr(msg);
	int a = stoi(zmsg_popstr(msg));
	int b = stoi(zmsg_popstr(msg));
	zmsg_t *content = zmsg_new();
	zmsg_prepend(content,&identity);
	zmsg_pushstr(content,to_string(a+b).c_str());
	zmsg_send(&content,server);
	zmsg_destroy(&content);
	zmsg_destroy(&msg);
	zframe_destroy(&identity);
	*/
	zmsg_t *out=zmsg_new();
	zmsg_addstr(out,"client1");
	zmsg_addstr(out,"Add");
	zmsg_addstr(out,"8");
	zmsg_addstr(out,"3");
	zmsg_addstr(out,"11");
	cout<<"envio al manager"<<endl;
	zmsg_print(out);
	zmsg_send(&out,server);
	zmsg_destroy(&out);
	zmsg_destroy(&msg);

}

void toBroker(void *socket, char *identity, char *op){
	zsocket_set_identity(socket,identity); 
	zsocket_connect(socket, "tcp://localhost:5556");
	zmsg_t *msg = zmsg_new();
	zmsg_addstr (msg, op);
	zmsg_send(&msg,socket);
	zmsg_destroy(&msg);
}

int main(int argc, char** argv) {
  zctx_t* context = zctx_new();
  void *responder = zsocket_new(context,ZMQ_DEALER);

	toBroker(responder,argv[1],argv[2]);
	zmq_pollitem_t items[] = {{responder, 0, ZMQ_POLLIN, 0}};
 

	while(true) {
		zmq_poll(items,1,10*ZMQ_POLL_MSEC);
    if(items[0].revents & ZMQ_POLLIN) {//soomething task from server;
			zmsg_t *msg = zmsg_recv(responder);
			dispatch(msg,responder);
			cout<<"salgooooo"<<endl;
    }
    //zmsg_t *msg = zmsg_recv(responder);
		///zmsg_print(msg);
    //dispatch(msg,responder);
  }
	zmq_close(responder);
	zctx_destroy(&context);
  return 0;
}
